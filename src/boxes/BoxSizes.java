package boxes;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import network.Network;
import network.Node;

/**
 * This program uses a layered network to solve a problem of selecting box
 * sizes for documents, so as to minimize the total surface area of all boxes
 * subject to a limit of either three or four distinct sizes.
 *
 * The question can be found at https://math.stackexchange.com/questions/
 * 2843990/need-to-create-3-4-different-box-sizes-and-to-minimize-material-
 * waste-for-a-se/2850260
 *
 * Documents are square, with the length of a side given in millimeters in
 * a file provided by the author of the question. That file is included in
 * the source code as a resource.
 *
 * The depth of a box is not specified, but assuming that boxes are shallow,
 * the surface area will be dominated by the (square) top and bottom. We use
 * the combined area of the top and bottom as our objective function here.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class BoxSizes {
  /** RAWDATA is the location of the raw data file. */
  private static final String RAWDATA = "../resources/documents_sides.txt";
  /** BOXES is the number of box sizes allowed. */
  private static final int BOXES = 4;

  /**
   * Dummy constructor (to pacify CheckStyle).
   */
  private BoxSizes() { }

  /**
   * @param args the command line arguments
   */
  public static void main(final String[] args) {
    long startTime = System.currentTimeMillis();
    // Start by reading all the document sizes into a list.
    List<String> data;
    try {
      URL url = BoxSizes.class.getResource(RAWDATA);
      Path path = Paths.get(url.toURI());
      data = Files.readAllLines(path);
    } catch (IOException | URISyntaxException ex) {
      System.out.println("Could not read the raw data file:\n"
                         + ex.getMessage());
      return;
    }
    System.out.println(data.size() + " sheet dimensions were read.");
    // Convert the text sizes to integer and map them to their frequencies.
    HashMap<Integer, Integer> frequency = new HashMap<>();
    for (String size : data) {
      int s = Integer.parseInt(size);
      if (frequency.containsKey(s)) {
        frequency.put(s, frequency.get(s) + 1);
      } else {
        frequency.put(s, 1);
      }
    }
    System.out.println("The number of distinct sizes is " + frequency.size()
                       + ".");
    // Build a layered network.
    Network net = new Network(BOXES, frequency);
    // Solve the network.
    long cost = net.solve();
    System.out.println("Total time spent = "
                       + (System.currentTimeMillis() - startTime) + " ms.");
    System.out.println("The total surface area (top and bottom only) of all"
                       + " boxes used in an optimal solution is "
                       + cost + " sq. mm.");
    Node[] path = net.getPath();
    System.out.println("Box sizes and counts used in the solution:");
    for (Node n : path) {
      int size = n.getSize();
      int count = n.getArc().getCount();
      System.out.println(String.format("\t%3d boxes with side %d mm.",
                                       count, size));
    }
  }

}
