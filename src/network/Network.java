package network;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Network implements a layered network.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Network {
  private final ArrayList<HashSet<Node>> layers;  // layers of the network
  private final int layerCount;                   // the number of layers
                                                  // (excluding layer 0)
  private final Node origin;                      // the origin node
  private final Node destination;                 // the destination node

  /**
   * Constructor.
   * @param nBoxSizes the number of box sizes allowed
   * @param frequencies the map of paper sizes to frequencies
   */
  public Network(final int nBoxSizes, final Map<Integer, Integer> frequencies) {
    // Create one (initially empty) layer for each allowed box size, plus a
    // layer zero for the start node.
    layerCount = nBoxSizes;
    layers = new ArrayList<>();
    for (int i = 0; i <= nBoxSizes; i++) {
      layers.add(new HashSet<>());
    }
    // Extract a sorted vector of document sizes from the frequency map.
    Integer[] docSize =
      frequencies.keySet()
                 .stream()
                 .sorted()
                 .toArray(dim -> new Integer[dim]);
    int nPaperSizes = docSize.length;
    // The start node is the sole occupant of the zero-th layer.
    origin = new Node(0);
    layers.get(0).add(origin);
    // The end layer contains just a node for the maximum document size.
    destination = new Node(docSize[nPaperSizes - 1]);
    layers.get(nBoxSizes).add(destination);
    // Intermediate layers contain nodes for all sizes greater than the smallest
    // size of the previous layer and less than the largest size of the next
    // layer.
    for (int layer = 1; layer < layerCount; layer++) {
      HashSet<Node> set = layers.get(layer);
      for (int i = layer - 1; i < nPaperSizes - nBoxSizes + layer; i++) {
        set.add(new Node(docSize[i]));
      }
    }
    // Finally, we need to connect every node in each layer (including the zero
    // layer) to every node in the following layer whose size is strictly
    // greater.
    for (int layer = 0; layer < nBoxSizes; layer++) {
      Set<Node> left = layers.get(layer);
      Set<Node> right = layers.get(layer + 1);
      for (Node a : left) {
        int sa = a.getSize();
        for (Node b : right) {
          int sb = b.getSize();
          if (sa < sb) {
            // Count documents with sizes strictly greater than sa and less
            // than or equal to sb.
            int count =
              frequencies.entrySet()
                         .stream()
                         .filter(e -> e.getKey() > sa && e.getKey() <= sb)
                         .collect(Collectors.summingInt(e -> e.getValue()));
            // Create the arc and associate it with the source node.
            a.addArc(new Arc(a, b, count, b.getArea()));
          }
        }
      }
    }
  }

  /**
   * Solve the shortest path problem using Dijkstra's algorithm.
   * @return the length of the shortest path (equal to the minimum total
   * surface area of all boxes used)
   */
  public long solve() {
    // Compile a list of nodes to examine, in layer order.
    LinkedList<Node> todo = new LinkedList<>();
    for (int layer = 0; layer <= layerCount; layer++) {
      todo.addAll(layers.get(layer));
    }
    // For every node other than the origin node, the initial predecessor is
    // null and the initial cost to reach it is infinite.
    for (int i = 1; i < todo.size(); i++) {
      Node x = todo.get(i);
      x.setPredecessor(null);
      x.setCostToReach(Long.MAX_VALUE);
    }
    // Now process nodes sequentially.
    while (!todo.isEmpty()) {
      // Pop the next node to process.
      Node source = todo.pop();
      // Get the cost to reach this node.
      long toReach = source.getCostToReach();
      // Examine each outbound arc at this node and see if it shortens the
      // distance to its sink.
      for (Arc a : source.getOutbound()) {
        Node sink = a.getSink();
        long oldCost = sink.getCostToReach();
        long newCost = toReach + a.getCost();
        if (oldCost > newCost) {
          sink.setCostToReach(newCost);
          sink.setPredecessor(source);
          sink.setArc(a);
        }
      }
    }
    // Return the cost to reach the destination node.
    return destination.getCostToReach();
  }

  /**
   * Get the optimal path through the network.
   * @return the optimal sequence of nodes
   */
  public Node[] getPath() {
    // Make sure the problem has been solved; if not, solve it now.
    if (destination.getPredecessor() == null) {
      solve();
    }
    Node[] path = new Node[layerCount];
    Node next = destination;
    // Build the path working backward from the destination.
    for (int i = layerCount - 1; i >= 0; i--) {
      path[i] = next;
      next = next.getPredecessor();
    }
    return path;
  }
}
