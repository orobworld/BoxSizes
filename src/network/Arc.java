package network;

/**
 * Arc represents an arc between nodes (in consecutive layers).
 *
 * The cost of an arc is the surface area of a box with side length given by
 * the sink node, multiplied by the total number of documents with sizes
 * greater than the size of the source node and less than or equal to the
 * size of the sink node.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Arc {
  private final Node source;   // the source node
  private final Node sink;     // the sink node
  private final long cost;     // the cost of the arc
  private final int count;     // number of documents covered by this arc

  /**
   * Constructor.
   * @param tail the source (tail) node
   * @param head the sink (head) node
   * @param boxes the number of boxes represented by this arc
   * @param area the surface area for each box
   */
  public Arc(final Node tail, final Node head,
             final int boxes, final int area) {
    source = tail;
    sink = head;
    count = boxes;
    cost = count * area;
  }

  /**
   * Get the source node.
   * @return the source node
   */
  public Node getSource() {
    return source;
  }

  /**
   * Get the sink node.
   * @return the sink node
   */
  public Node getSink() {
    return sink;
  }

  /**
   * Get the arc cost.
   * @return the arc cost
   */
  public long getCost() {
    return cost;
  }

  /**
   * Get the number of boxes represented by this arc.
   * @return the box count
   */
  public int getCount() {
    return count;
  }

}
