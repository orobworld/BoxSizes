package network;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 * Node represents a node (box size) in our network.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Node {
  private final int size;               // dimension of one side
  private final int area;               // surface area
  private final HashSet<Arc> outbound;  // arcs outbound from this node
  // Fields used by Dijkstra's algorithm:
  private Node predecessor;             // previous node along shortest path
  private long costToReach;             // cost to reach this node
  private Arc arc;                      // the solution arc leading to this node

  /**
   * Constructor creating a new node with an empty set of outbound arcs.
   * @param s the edge size
   */
  public Node(final int s) {
    size = s;
    area = 2 * s * s;
    outbound = new HashSet<>();
  }

  /**
   * Add an outbound arc at this node.
   * @param a the new arc
   */
  public void addArc(final Arc a) {
    outbound.add(a);
  }

  /**
   * Get the edge dimension at this node.
   * @return the edge dimension
   */
  public int getSize() {
    return size;
  }

  /**
   * Get the combined top and bottom area at this node.
   * @return the area
   */
  public int getArea() {
    return area;
  }

  /**
   * Get a list of the outbound arcs at this node.
   * @return an unmodifiable list of the outbound arcs
   */
  public Collection<Arc> getOutbound() {
    return Collections.unmodifiableCollection(outbound);
  }

  /**
   * Get the predecessor node.
   * @return the previous node on the shortest path to this one
   */
  public Node getPredecessor() {
    return predecessor;
  }

  /**
   * Set the predecessor node.
   * @param pred the new predecessor
   */
  public void setPredecessor(final Node pred) {
    predecessor = pred;
  }

  /**
   * Get the cost to reach this node on the shortest path to it.
   * @return the cost to reach here
   */
  public long getCostToReach() {
    return costToReach;
  }

  /**
   * Set the cost to reach this node.
   * @param cost the new cost
   */
  public void setCostToReach(final long cost) {
    costToReach = cost;
  }

  /**
   * Get the arc that arrives at this node in the optimal path.
   * @return the incoming arc
   */
  public Arc getArc() {
    return arc;
  }

  /**
   * Set the incoming arc on the shortest path to this node.
   * @param inbound the incoming arc
   */
  public void setArc(final Arc inbound) {
    arc = inbound;
  }

}
